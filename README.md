# pkg-tasktools

Team repository

## Links:

* <https://salsa.debian.org/tasktools-team/>
* <https://lists.alioth.debian.org/mailman/listinfo/pkg-tasktools-discuss>

## Create a Gitlab token

* <https://salsa.debian.org/profile/personal_access_tokens>

Then create a salsarc file with :

    # salsarc
    SALSA_TOKEN="blablabla"

## Checkout

To check out all pkg-tasktools packages, do the following:

    git clone git@salsa.debian.org:tasktools-team/pkg-tasktools.git tasktools-team

Add the following line to ~/.mrtrust

    ~/debian/tasktools-team/.mrconfig

And finally run mr:

    cd ~/debian/tasktools-team
    mr -f -j 10 update

## New package

1: Make a package repository locally

    ./setup-project

2: Push your changes

3: Add package repo stanza to tasktools-team's .mrconfig with

    ./update-mrconfig
    git add .mrconfig .gitignore
    git commit -m 'meta: refresh mrconfig (new repo)'
    git push
