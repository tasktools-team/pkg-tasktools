require "gitlab"

load(File.expand_path(File.dirname(__FILE__)+"/../salsarc"))
#SALSA_TOKEN = ENV["GITLAB_API_PRIVATE_TOKEN"] || (File.exists?("salsa.token") && File.read("salsa.token").strip)
# curl --request GET -d "search=tasktools-team" -s https://salsa.debian.org/api/v4/groups | jq '.[].id'
SALSA_NAMESPACE=2573 # tasktools-team group
SALSA_GROUP="tasktools-team"
SALSA_URL="https://salsa.debian.org/api/v4"

Gitlab.endpoint=SALSA_URL
Gitlab.private_token=SALSA_TOKEN

def create_project(name)
  begin
    project=Gitlab.create_project(name, {description: "#{name} packaging", namespace_id: SALSA_NAMESPACE})
    puts "I #{name}: repository created (id: #{project.id})"
    return project
  rescue Gitlab::Error::Error => error
    puts error
    puts "E #{name}: repository creation failed. Probably already exists."
    begin
      project=Gitlab.project([SALSA_GROUP,name].join("/"))
    rescue Gitlab::Error::Error => error
      puts error
      puts "E #{name}: repository not found. Stopping here."
      exit(1)
    end
  end
end

def set_irc_notifications(project)
  Gitlab.change_service(project.id, :irker,
                          "recipients=debian-task&default_irc_uri=irc://irc.oftc.net:6667/&server_host=ruprecht.snow-crash.org&server_port=6659&colorize_messages=true")
    puts "I #{project.name}: irc notification activated"
end

def set_webhooks(project)
  #tagpending
  tagpending_url="https://webhook.salsa.debian.org/tagpending/#{project.name}"
  if Gitlab.project_hooks(project.id).all? {|hook| hook.url != tagpending_url}
    Gitlab.add_project_hook(project.id, tagpending_url, {push_events: true})
    puts "I #{project.name}: tagpending webhook activated"
  else
    puts "W #{project.name}: tagpending webhook already enabled"
  end
end

def team_projects
  Gitlab.group(SALSA_NAMESPACE).projects
end
